use std::fmt::Debug;

trait DataSource : Debug {
    fn was_network_error(&self) -> bool;
}

fn process<T: ?Sized + DataSource>(input: &T) -> i32
// can also be written as:
// fn process(input: &(impl DataSource + ?Sized)) -> i32
{
    let mut sum_errors = 0;
    for _i in 0..1000000 {
        if input.was_network_error() {
            sum_errors += 1;
        }
    }
    sum_errors
}


#[derive(Debug)]
struct FilesystemObject {
    inode: i32,
}

impl DataSource for FilesystemObject {
    fn was_network_error(&self) -> bool { false }
}


extern crate rand;
use rand::Rng;

#[derive(Debug)]
struct NetworkObject {
    address: i32,
}

impl DataSource for NetworkObject {
    fn was_network_error(&self) -> bool
    {
        let mut rng = rand::thread_rng();
        rng.gen()
    }
}



extern crate time;

fn main() {
    let f = FilesystemObject { inode: 42 };
    let start = time::PreciseTime::now();
    println!("Network errors: {}", process(&f));
    let end = time::PreciseTime::now();
    println!("Took {}", start.to(end));

    let n = NetworkObject { address: 42 };
    let start = time::PreciseTime::now();
    println!("Network errors: {}", process(&n));
    let end = time::PreciseTime::now();
    println!("Took {}", start.to(end));


    let mut rng = rand::thread_rng();
    let any: &dyn DataSource = if rng.gen() {
        &f
    } else {
        &n
    };
    println!("Any is {:?}",any);

    let start = time::PreciseTime::now();
    println!("Environment errors: {}", process(any));
    let end = time::PreciseTime::now();
    println!("Took {}", start.to(end));
}
